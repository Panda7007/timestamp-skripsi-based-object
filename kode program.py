import cv2
import numpy as np
import time
import datetime as dt
import os
import imageio
import requests
import argparse

# Buat parser argumen
'''
parser = argparse.ArgumentParser(description="Contoh menerima argumen dari command line")
# Tambahkan argumen
parser.add_argument('--nama', type=str, help="Nama pengguna")
# Parsing argumen
args = parser.parse_args()
'''
# Mengambil gambar dari sumber (sumber dari obs pada port 6)
cap = cv2.VideoCapture(f"lokasi file")

# Mendefinisikan path lengkap atau relatif ke file model ONNX
nama_onnx = "humanversi5"
model_path = os.path.join("model", f"{nama_onnx}.onnx")

# Memuat model ONNX
net = cv2.dnn.readNetFromONNX(model_path)

# Buat menandai class berdasarkan urutan class di roboflow
classes = [nama_onnx]
tanggal = dt.datetime.now().date()

# Define the codec and create VideoWriter object
fpsvideo = 5
output_folder = "1. Video hasil"
if not os.path.exists(output_folder):
    os.makedirs(output_folder)
output_video_path = os.path.join(output_folder, f'video-hasil.mp4')

# Buat objek writer dengan menggunakan ImageIO
out = imageio.get_writer(output_video_path, fps=fpsvideo)

# Settingan untuk melihat FPS
starting_time = time.time()
frame_id = 0
font = cv2.FONT_HERSHEY_COMPLEX

# Mendefinisikan path file output di dalam folder khusus
output_file_folder = "1.timestamp hasil"
if not os.path.exists(output_file_folder):
    os.makedirs(output_file_folder)
output_file = os.path.join(output_file_folder, f"timestamp-video-hasil.txt")

# Membuat folder bukti_foto jika belum ada
bukti_foto_folder = "bukti_foto"
if not os.path.exists(bukti_foto_folder):
    os.makedirs(bukti_foto_folder)

# Mendefinisikan selisih timelapse pertama dengan selanjutnya
stamp1 = 0
stampout = 0

# Untuk kondisional alarm
jam = dt.datetime.now().hour

# Text caption to be sent along with the photo
caption_text = "Terdeteksi gerakan di kamera"
data = {'chat_id': '-4062304369', 'caption': caption_text}

while True:
    ret, img = cap.read()
    if not ret:
        break
    frm = img.copy()
    frame_id += 1

    img = cv2.resize(img, (720, 480))
    blob = cv2.dnn.blobFromImage(img, scalefactor=1/255, size=(640, 640), mean=[0, 0, 0], swapRB=True, crop=False)
    net.setInput(blob)
    detections = net.forward()[0]

    classes_ids = []
    confidences = []
    boxes = []
    rows = detections.shape[0]

    img_width, img_height = img.shape[1], img.shape[0]
    x_scale = img_width / 640
    y_scale = img_height / 640

    for i in range(rows):
        row = detections[i]
        confidence = row[4]
        if confidence >= 0.25:
            classes_score = row[5:]
            ind = np.argmax(classes_score)
            if classes_score[ind] >= 0.25:
                classes_ids.append(ind)
                confidences.append(confidence)
                cx, cy, w, h = row[:4]
                x1 = int((cx - w / 2) * x_scale)
                y1 = int((cy - h / 2) * y_scale)
                width = int(w * x_scale)
                height = int(h * y_scale)
                box = np.array([x1, y1, width, height])
                boxes.append(box)

    indices = cv2.dnn.NMSBoxes(boxes, confidences, 0.25, 0.25)

    waktu = time.asctime()
    jam = dt.datetime.now().hour
    tanggal = dt.datetime.now().date()

    elapsed_time = time.time() - starting_time
    fps = frame_id / elapsed_time

    cv2.putText(img, "Jumlah Orang : " + str(len(indices)), (10, 90), font, 0.4, (200, 0, 0), 1)
    cv2.putText(img, "Fps : " + str(fps), (10, 110), font, 0.3, (255, 0, 0), 1)
    cv2.putText(img, "Waktu : " + str(waktu), (10, 50), font, 0.3, (200, 0, 0), 1)
    cv2.putText(img, "Tekan Q untuk keluar & Save ", (10, 280), font, 0.3, (200, 200, 200), 1)
    cv2.putText(img, "Jam : " + str(jam), (10, 70), font, 0.3, (200, 0, 0), 1)

    

    # Boxing object yang terdeteksi
    for i in indices:
        x1, y1, w, h = boxes[i]
        label = classes[classes_ids[i]]
        conf = confidences[i]
        text = label + "{:.2f}".format(conf)
        cv2.rectangle(img, (x1, y1), (x1 + w, y1 + h), (255, 255, 0), 1)
        cv2.putText(img, text, (x1, y1 - 2), cv2.FONT_HERSHEY_COMPLEX, 0.2, (0, 255, 255), 1)
        cv2.rectangle(frm, (x1, y1), (x1 + w, y1 + h), (255, 0, 0), 2)
        cv2.putText(frm, text, (x1, y1 - 2), cv2.FONT_HERSHEY_COMPLEX, 0.2, (0, 0, 255), 1)
        

    out.append_data(img)
    
    cv2.imshow("VIDEO", img)

    # Path folder untuk menyimpan bukti terdeteksi
    output_folder = "bukti_terdeteksi"
    # Deteksi manusia untuk menjalankan alarm
    
    if len(indices) > stamp1:
    # Check if the current detik is different from the previous one
        
        # Simpan nomor frame yang terdeteksi ke dalam file teks
        with open(output_file, 'a') as file:
            file.write(str(int(detik)) + '\n')
        print("Posisi frame yang tercatat dalam database:", int(frame_id))
        
        detected_frames = []

        stamp1 = len(indices)
    
    if stamp1 > len(indices):
        stamp1 = len(indices)

    if len(indices) < stampout:
        # Check if the current detik is different from the previous one
            # Simpan nomor frame yang terdeteksi ke dalam file teks
            
        with open(output_file, 'a') as file:
            file.write(str(int(frame_id)) + '\n')
        print("Posisi frame yang tercatat dalam database stampout:", int(frame_id))
        
        detected_frames = []
        stampout = len(indices)

    if stampout < len(indices):
        stampout = len(indices)

    print("Posisi frame saat ini:", frame_id)
    print("Jumlah Orang:", len(indices))
    print("Jumlah orang untuk out:", stampout)




    k = cv2.waitKey(10)
    if k == ord('q'):
        break

# Release the capture and writer objects
cap.release()

# Destroy all windows
cv2.destroyAllWindows()

# Release the writer object
out.close()
